import React, { useEffect, useState } from 'react'
import './App.css'
import TodoInput from './components/TodoInput.js';
import TodoItem from './components/TodoItem.js';

const App = () => {

  const [tasksRemaining, setTasksRemaining] = useState(0);
  const [todoItems, setTodoItems] = React.useState(
    [{
      todo: 'Mow the lawn',
      complete: false
    },
    {
      todo: 'Do Laundry',
      complete: false
    },
    {
      todo: 'Make Dinner',
      complete: false
    }])

  useEffect(() => { 
    setTasksRemaining(todoItems.filter(todo => !todo.complete).length) 
  }, [todoItems]);

  const createTodoItem = (todo) => {
    const newTodoItems = [...todoItems, { todo, complete: false }];
    setTodoItems(newTodoItems);
  }

  const deleteTodoItem = (index) => {
    const newTodoItems = [...todoItems]
    newTodoItems.splice(index, 1)
    setTodoItems(newTodoItems)
  }

  const completeTodoItem = (index) => {
    const newTodoItems = [...todoItems];
    newTodoItems[index].complete === false
      ? (newTodoItems[index].complete = true)
      : (newTodoItems[index].complete = false);
    setTodoItems(newTodoItems)
  };

  const updateTodoItem = (index) => {
    const newTodoItems = [...todoItems];
    const item = newTodoItems[index];
    let newItem = prompt(`Update ${item.todo}?`, item.todo);
    let todoObj = { todo: newItem, complete: false };
    newTodoItems.splice(index, 1, todoObj);
    if (newItem === null || newItem === "") {
      return;
    } else {
      item.todo = newItem;
    }
    setTodoItems(newTodoItems);
  };


  return (
    <div className='app'>
      
      <TodoInput createTodoItem={createTodoItem} />
      <div className="header">Pending Tasks ({tasksRemaining})</div>
      {todoItems.map((item, index) => (
        <TodoItem key={index} index={index} item={item} deleteTodoItem=
          {deleteTodoItem} completeTodoItem={completeTodoItem}
          updateTodoItem={updateTodoItem} />
      ))}
    </div>
  );
}

export default App