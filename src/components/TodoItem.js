const TodoItem = ({item, index, deleteTodoItem, completeTodoItem, updateTodoItem}) => {
    return (
    <div className="todo-list">
    <li style={{textDecoration: item.complete ? "line-through" : ""}}>{item.todo}</li>
    <div className="btns">
    <button className="button-18" onClick={() => completeTodoItem(index)}>Complete</button>
    <button className="button-18" onClick={() => updateTodoItem(index)}>Update</button>
    <button className="button-18" onClick={() => deleteTodoItem(index)}>X</button>
    </div>
    </div>
    )};
    export default TodoItem